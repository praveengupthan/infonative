<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Infonative</title>
        <!-- styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/icomoon.css">
        <link rel="stylesheet" href="css/style.css">
        <?php include 'objectArray.php'?>
    </head>    
    <body>

        <!-- question section -->
        <div class="questionSection">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="qBlock text-center">
                            <p>Question</p>
                            <h6 class="h5 fbold">01/02</h6>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="qdesc">
                            <p>Below are three options on how to respond to the scenario above.  Image question the client side pings you and tell that they have a personal emergency and will need to be away from work.  </p>
                        </div>
                        <!-- row -->
                        <div class="row py-3">
                            <?php
                            for ($i=0; $i<count($options); $i++) {?>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="optionCol text-center">
                                    <a href="javascript:void(0)">
                                        <img src="img/<?php echo $options[$i][0] ?>.png" alt="" class="img-fluid mb-2">
                                    </a>
                                    <input type="checkbox" name="">
                                </div>
                            </div>
                           <?php } ?>
                        </div>
                        <!--/ row -->
                        <div class="text-center py-4">
                            <button class="btn btn-success mx-auto">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ question section -->





        

        <!-- script files -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>
        <!-- [if let IE 9]>
        <script src="js/ie.lteIE9.js"></script>
    <![endif]-->
    </body>
</html>